#!/usr/bin/env python3.7

"""
Test tool to generate log spam
"""

import datetime
import os
import random
import time

# Tool will write out a random line from here
sample_lines = [
    "Fake Perforce block header:",
    "Testing 1 2 3",
    [
        "Perforce message:",
        "\tThing 1",
        "\tThing 2",
        "\tThing 3",
    ],
    None,
    "someone is connecting",
    "do the thing",
    "what?",
]


def heartbeat_delay(tick_delay_seconds, max_seconds):
    """
    Generator to give a slight pause, or heartbeat to our activities
    up to, but not exceeding, the max_seconds
    """
    if max_seconds:
        start = datetime.datetime.utcnow()
        stop = start + datetime.timedelta(seconds=max_seconds)
        while stop > datetime.datetime.utcnow():
            time.sleep(tick_delay_seconds)
            yield True
    else:
        # no max, go forever
        while True:
            time.sleep(tick_delay_seconds)
            yield True


def main():
    """
    traditional entry point of my people
    """
    log_file = "log.log"
    write_delay_seconds = 0.2
    file_lifetime_seconds = 20

    with open("logspam.pid", 'wt') as obj:
        obj.write(str(os.getpid()))

    # Cycle between write modes
    mode_list = [
        "wt",
        "at"
    ]

    while True:
        for mode in mode_list:
            print(f"Opening {log_file} as [{mode}]\n-----------------")
            with open(log_file, mode) as output:
                output.write(f"--------- new file handle, mode [{mode}] ---------\n")
                for _ in heartbeat_delay(write_delay_seconds, file_lifetime_seconds):
                    # pick a random line to write out
                    line = random.choice(sample_lines)
                    if isinstance(line, str):
                        print("line")
                        output.write(line + '\n')
                    elif isinstance(line, list):
                        print("LIST")
                        output.write('\n'.join(line) + '\n')
                    else:
                        line = str(datetime.datetime.now())
                        print(line)
                        output.write(line + '\n')

                    #
                    output.flush()

            print("-------------")
            print(f"Final file size of [{log_file}] = {os.path.getsize(log_file)}")


if __name__ == '__main__':
    try:
        main()
    except:
        print("")
