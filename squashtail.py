#!/usr/bin/env python3.7

"""
Problem: p4d outputs noisy multi-line log lines that flood AWS CloudWatch & hurt readability
Solution: consolidate some of them into a single "print"

Existing log lines
1     text
2     text
3     Perforce message:
4     \tmessage
5     \tmessage
6     text
7     text

Instead of "read a line, print a line" 7x. it will call print 5x
lines 3-4-5 will be printed in 1 print call & with intermediate newlines removed

"""

import subprocess
import queue
import sys
import threading


def main():
    """traditional entry point of my people"""

    if not sys.argv or len(sys.argv) < 2:
        print(f"no application to launch was specified on the command line {sys.argv}")
        print("Usage: quashtail.py tail -F log.log")
        sys.exit(1)

    tail_cmd = sys.argv[1:]
    que_of_strings = queue.Queue()
    event_shutdown = threading.Event()

    def worker():
        buffer = ""

        while not event_shutdown.isSet():
            line = que_of_strings.get()
            if not line:
                continue

            # flush the previous line
            if line.endswith(':\n') and buffer:
                print(buffer, flush=True, end='')
                buffer = ""

            if line.endswith(':\n') or line.startswith('\t'):
                print_it = False
                buffer = buffer.rstrip()
            else:
                print_it = True

            buffer += line
            if print_it and buffer:
                print(buffer, flush=True, end='')
                buffer = ""

    threading.Thread(target=worker).start()

    child = subprocess.Popen(tail_cmd,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

    try:
        while True:
            line = child.stdout.readline().decode()
            que_of_strings.put(line)
    except:
        pass

    # stop the worker
    event_shutdown.set()


if __name__ == '__main__':
    main()
