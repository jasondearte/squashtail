# squashtail

Simple POC

My tool to consolidate verbose text output blocks from Helix Core into a single line.   
Handy for keeping your "tail -f $P4ROOT/log" output more manageable in AWS Cloudwatch.  

This is not for everyone, but it makes it easier for me to filter out the AWS EBS healthcheck spam

## Normal Perforce output

```bash
Perforce message:
	Thing 1
	Thing 2
	Thing 3
Testing 1 2 3
Perforce message:
	Thing 1
	Thing 2
	Thing 3
Perforce message:
	Thing 1
	Thing 2
	Thing 3
```

## Improved Perforce output
```bash
Perforce message:	Thing 1	Thing 2	Thing 3
Testing 1 2 3
Perforce message:	Thing 1	Thing 2	Thing 3
Perforce message:	Thing 1	Thing 2	Thing 3
```

# Testing

1. run `./logspam.py` to continuously write to `log.log`
2. in a terminal window run a normal tail command  
    `tail -F ./log.log`
3. in another terminal window, run with the squashtail modifier  
    `./quashtail.py tail -F ./log.log`
4. compare the outputs

